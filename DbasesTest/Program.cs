﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbasesTest
{

    partial class Category
    {
        public override string ToString()
        {
            return $"{this.CategoryID}: {this.CategoryName}";
        }
    }
    class Program


        
    {
        static void Main(string[] args)
        {
            DataCDataContext data = new DataCDataContext();
            data.Log = Console.Out;


            var products = data.Products.ToList();

            foreach (var c in data.Categories)
            {
                foreach (var p in data.Products)
                {
                    Console.WriteLine($"{ p.ProductName} belong into group {c.CategoryName}");
                }
            }


            var q = from p in data.Products
                    join c in data.Categories
                    on p.CategoryID equals c.CategoryID
                    select new { c.CategoryName, p.ProductName };

            foreach (var x in q)
            {
                Console.WriteLine(x);
            }

        }
    }
}
